Rails.application.routes.draw do
  root 'page#index'

  devise_for :users, path: "acesso", path_names: { sign_up: 'cadastrar', sign_in: 'entrar', sign_out: 'sair', password: 'secret' }

  scope(path_names: { new: 'novo', edit: 'editar', show: 'ver' }) do
    resources :people
    resources :users
  end
end
